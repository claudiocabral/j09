/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rot42.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/10 20:47:24 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 21:40:17 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_rot42(char *str)
{
	unsigned char	a;
	char			*head;

	head = str;
	while (*str)
	{
		if (*str >= 'a' && *str <= 'z')
			*str = (*str - 'a' + 16) % 26 + 'a';
		else if (*str >= 'A' && *str <= 'Z')
			*str = (*str - 'A' + 16) % 26 + 'A';
		++str;
	}
	return (head);
}
