/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unmatch.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 14:02:14 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 14:23:14 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_unmatch(int *tab, int length)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < length)
	{
		j = 0;
		while (j < length)
		{
			if (tab[i] == tab[j] && i != j)
			{
				break ;
			}
			++j;
		}
		if (j == length)
			return (tab[i]);
		++i;
	}
	return (tab[i]);
}
