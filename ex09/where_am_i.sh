#!/bin/sh
RESULT=`ifconfig | grep -oE 'inet ([0-9]{1,3}.){3}[0-9]{1,3}' | grep -v 127.0.0.1 | awk '{print $2}'`
if [[ -z $RESULT ]] ; then
    echo "Je suis perdu!"
else
    echo $RESULT | tr ' ' '\n'
fi
