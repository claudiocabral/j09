/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/10 18:03:51 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/10 22:16:29 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_print_hour(int hour)
{
	if (hour == 0 || hour == 24)
		printf("12.00 A.M.");
	else if (hour == 12)
		printf("12.00 P.M.");
	else if (hour > 12)
		printf("%d.00 P.M.", hour % 12);
	else
		printf("%d.00 A.M.", hour);
}

void	ft_takes_place(int hour)
{
	printf("THE FOLLOWING TAKES PLACE BETWEEN ");
	ft_print_hour(hour);
	printf("AND ");
	ft_print_hour(hour + 1);
	printf("\n");
}
