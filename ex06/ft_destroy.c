/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destroy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:52:52 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 16:48:59 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ultimator.h"

void	ft_destroy(char ***factory)
{
	while (*factory)
	{
		while(**factory);
		{
			free(**factory);
			++(**factory);
		}
		free(*factory);
		++(*factory);
	}
	if (factory)
		free(factory);
}
