/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 10:06:39 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 10:26:35 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strlowcase(char *str)
{
	char	*head;

	head = str;
	while (*str)
	{
		if (*str >= 'A' && *str <= 'Z')
			*str += 32;
		++str;
	}
	return (head);
}

int		ft_strcmp(char *s1, char *s2)
{
	unsigned char	a;
	unsigned char	b;

	while (*s1 == *s2)
	{
		if (*s1 == '\0' || *s1 == ' '
				|| *s1 == '\t' || *s1 == '\n')
			break ;
		++s1;
		++s2;
	}
	a = *s1;
	b = *s2;
	return (a - b);
}

int		main(int argc, char **argv)
{
	int	i;

	i = 1;
	while (i < argc)
	{
		ft_strlowcase(argv[i]);
		while (*(argv[i]))
		{
			while (*(argv)[i] == ' ' || *(argv[i]) == '\t'
					|| *(argv[i]) == '\n')
				++(argv[i]);
			if (ft_strcmp(argv[i], "president") == 0
					|| ft_strcmp(argv[i], "attack") == 0
					|| ft_strcmp(argv[i], "powers") == 0)
				write(1, "Alert!!!\n", 9);
			while ((*(argv)[i] != ' ' || *(argv[i]) != '\t'
					|| *(argv[i]) != '\n') && *(argv[i]))
				++(argv[i]);
		}
		++i;
	}
	return (0);
}
