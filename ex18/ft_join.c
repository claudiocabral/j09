/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_join.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 13:45:12 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 14:01:16 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlenj07ex03(char *str)
{
	int	length;

	length = 0;
	while (*str)
	{
		++length;
		++str;
	}
	return (length);
}

char	*ft_strcatj07ex03(char *dest, char *src)
{
	char	*head;

	head = dest;
	while (*dest)
		++dest;
	while (*src)
	{
		*dest = *src;
		++dest;
		++src;
	}
	*dest = '\0';
	return (head);
}

int		ft_accumulate(char **tab, char *sep)
{
	int	size;
	int	sep_size;
	int	i;

	i = 0;
	size = 0;
	sep_size = ft_strlenj07ex03(sep);
	while (*tab)
	{
		size += ft_strlenj07ex03(*tab);
		size += sep_size;
		++tab;
	}
	++size;
	return (size);
}

char	*ft_join(char **tab, char *sep)
{
	char	*str;
	int		i;
	long	size;

	size = ft_accumulate(tab, sep);
	if (size)
		str = (char *)malloc(sizeof(char) * size);
	else
	{
		str = (char *)malloc(sizeof(char));
		*str = '\0';
		return (str);
	}
	i = 0;
	while (*(tab + 1))
	{
		ft_strcatj07ex03(str, *tab);
		ft_strcatj07ex03(str, sep);
		++tab;
	}
	ft_strcatj07ex03(str, *tab);
	return (str);
}
