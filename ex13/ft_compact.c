/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_compact.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 10:27:50 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 11:22:22 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_compact(char **tab, int length)
{
	int	new_size;
	int	i;

	new_size = 0;
	i = 0;
	while (i < length)
	{
		if (tab[i])
			++new_size;
		++i;
	}
	while (length > 0)
	{
		if (!*tab)
		{
			i = 1;
			while (tab[i] == 0 && i < length)
				++i;
			*tab = tab[i];
			tab[i] = 0;
		}
		++tab;
		--length;
	}
	return (new_size);
}
