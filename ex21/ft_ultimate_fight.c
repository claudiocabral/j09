/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fight.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:28:43 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 14:54:16 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ultimate_fight.h"
#include <stdio.h>

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		++str;
	}
}

void	ft_print_dead(t_perso *dead)
{
	ft_putstr(dead->name);
	ft_putstr(" is dead\n");
}

void	ft_attack(t_perso *defense, int attack)
{
	if (attack != KICK10 && attack != KICK11)
		defense->life -= attack;
	else if (attack == KICK10)
		defense->life -= 9;
	else if (attack == KICK11)
		defense->life -= 18;
	if (attack == PUNCH)
		ft_putstr(" punch on ");
	else if (attack == HEADBUTT)
		ft_putstr(" headbutt on ");
	else
		ft_putstr(" kick on ");
}

void	ft_fight(t_perso *attacker, t_perso *defense, int attack)
{
	if (attacker->life <= 0 && defense->life <= 0)
		return ;
	ft_putstr(attacker->name);
	ft_putstr(" does a judo ");
	ft_attack(defense, attack);
	ft_putstr(defense->name);
	ft_putstr(".\n");
	if (defense->life <= 0)
		ft_print_dead(defense);
}
