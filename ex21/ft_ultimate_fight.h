/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fight.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:27:09 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 14:43:21 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ULTIMATE_FIGHT_H
# define FT_ULTIMATE_FIGHT_H

# include "ft_perso.h"
# include <unistd.h>

enum	e_attacks
{
	PUNCH = 15,
	HEADBUTT = 20,
	KICK = 5,
	KICK1 = 13,
	KICK2 = 3,
	KICK3 = 18,
	KICK4 = 9,
	KICK5 = 11,
	KICK6 = 13,
	KICK7 = 8,
	KICK8 = 6,
	KICK9 = 11,
	KICK10 = 90,
	KICK11 = 180,
	KICK12 = 10
};

void	ft_fight(t_perso *attacker, t_perso *defense, int attack);

#endif
