/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fight.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:28:43 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 14:27:42 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fight.h"

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		++str;
	}
}

void	ft_print_dead(t_perso *dead)
{
	ft_putstr(dead->name);
	ft_putstr(" is dead\n");
}

void	ft_fight(t_perso *attacker, t_perso *defense, int attack)
{
	if (attacker->life <= 0 && defense->life <= 0)
		return ;
	ft_putstr(attacker->name);
	ft_putstr(" does a judo ");
	if (attack == KICK)
	{
		defense->life -= 15;
		ft_putstr("kick on ");
	}
	else if (attack == PUNCH)
	{
		defense->life -= 5;
		ft_putstr("punch on ");
	}
	else if (attack == HEADBUTT)
	{
		defense->life -= 20;
		ft_putstr("headbutt on ");
	}
	ft_putstr(defense->name);
	ft_putstr(".\n");
	if (defense->life <= 0)
		ft_print_dead(defense);
}
