/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fight.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:27:09 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 13:12:34 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FIGHT_H
# define FT_FIGHT_H

# include "ft_perso.h"
# include <unistd.h>

# define KICK 0
# define PUNCH 1
# define HEADBUTT 2

void	ft_fight(t_perso *attacker, t_perso *defense, int attack);

#endif
