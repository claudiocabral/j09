/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_decrypt.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 12:08:44 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 13:07:34 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_perso.h"
#include <stdlib.h>
#include <stdio.h>

int		ft_atoi(char *str)
{
	int		signal;
	long	num;

	num = 0;
	signal = 1;
	while (*str <= 32)
		++str;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			signal *= -1;
		++str;
	}
	while ((*str >= '0') && (*str <= '9'))
	{
		num *= 10;
		num += (*str - 48);
		++str;
	}
	num *= signal;
	return (num);
}

char	*ft_strdup(char *src, char separator)
{
	char	*head;
	char	*dup;
	int		size;

	head = src;
	size = 1;
	while (*src && *src != separator)
	{
		++size;
		++src;
	}
	dup = (char *)malloc(sizeof(char) * size);
	src = head;
	head = dup;
	while (*src && *src != separator)
		*dup++ = *src++;
	*dup = '\0';
	return (head);
}

t_perso	*ft_make_person(char *str)
{
	t_perso	*person;

	person = malloc(sizeof(t_perso));
	while (*str == ';' && *str)
		++str;
	if (*str && *(str + 1) != '|')
		person->age = ft_atoi(str);
	while (*str && *str != '|')
		++str;
	if (*str)
		++str;
	if (*str && *str != ';')
		person->name = ft_strdup(str, ';');
	return (person);
}

t_perso	**ft_decrypt(char *str)
{
	t_perso	**person;
	int		size;
	int		i;

	size = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] == ';' || str[i] == '|')
			++i;
		while (str[i] != ';' && str[i])
			++i;
		++size;
	}
	person = malloc(sizeof(t_perso *) * (size + 1));
	i = -1;
	while (++i < size)
	{
		person[i] = ft_make_person(str);
		while (*str && *str != ';')
			++str;
		(*str) ? (++str) : (str += 0);
	}
	person[i] = 0;
	return (person);
}
