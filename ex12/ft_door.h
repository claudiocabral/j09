/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_door.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 12:00:10 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 12:04:57 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DOOR_H
# define FT_DOOR_H

# include <unistd.h>

# define TRUE 1
# define FALSE 0
# define EXIT_SUCCESS 0
# define OPEN 0
# define CLOSE 1

typedef int		t_bool;

typedef struct	s_door
{
	int state;
}				t_door;

#endif
