/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_active_bits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 13:13:31 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 13:39:24 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_active_bits(int value)
{
	long			i;
	unsigned int	total;

	i = 2;
	total = 0;
	total += value & 1;
	while (i < 0x80000000)
	{
		if ((value & i))
			++total;
		i *= 2;
	}
	if ((value & i))
		++total;
	return (total);
}
