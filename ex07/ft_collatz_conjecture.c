/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjecture.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:53:34 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/11 11:54:15 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	if (base == 0)
		return (0);
	if (base == 1)
		return (1);
	if (!(base % 2))
		return (1 + (ft_collatz_conjecture(base / 2)));
	return (1 + (ft_collatz_conjecture(base * 3 + 1)));
}
